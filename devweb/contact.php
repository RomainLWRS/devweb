<?php
    require_once 'php/connexion.php';
    require_once 'php/header.php';
    require_once 'php/head.php';
    require_once 'php/footer.php';
    require_once 'php/leftNavbar.php';
    require_once 'php/varSession.inc.php';
    require_once 'php/db.php';
    //require_once 'php/panier.php';
?>

<!DOCTYPE html>
<html>

<body>
    <div class="mainContact">
		<form id="contactForm" method="post" enctype="text/plain" action="mailto:kiroule.com@contact.fr">
            <br>
            <label for="dateContact">Date de contact </label>
                <input type="date" name="dateContact" id="DateContact" required> <br>

            <label for="Nom"> Nom </label>
                <input type="text" name="Nom" id="Nom" placeholder="Entrez votre nom" required> <br>

            <label for="Prenom"> Prénom </label>
                <input type="text" name="Prenom" id="Prenom" placeholder="Entrez votre prénom" required> <br>

            <label for="email"> Email </label>
                <input type="email" name="email" id="email" placeholder="Entrez votre adresse mail : mon@email.com" required> <br>

            <div class="choixSexe">
            <label for="Sexe">Sexe</label> 
                <input type="radio" name="Sexe" id="Femme"
                <label for="f">Femme</label> 
                <input type="radio" name="Sexe" id="Homme"
                <label for="h">Homme</label>  <br>  <br> 
            </div>

            <label for="dateNaissance">Date de naissance </label>
                <input type="date" name="dateNaissance" id="dateNaissance"> <br>
                
            <label for="Pays"> Pays </label>
                <div class="selectPays">
                    <select name="Pays">
                        <option value="France">France</option>
                        <option value="Espagne">Espagne</option>
                        <option value="Italie">Italie</option>
                        <option value="Royaume-uni">Royaume-Uni</option>
                        <option value="Canada">Canada</option>
                        <option value="Etats-Unis">États-Unis</option>
                        <option value="Chine">Chine</option>
                        <option value="Japon">Japon</option>
                    </select> <br>
                </div>
            <label for="Sujet"> Objet :</label>
                <input type="text" name="Sujet" id="Sujet" placeholder="Entrez l'objet de votre mail" required> <br>

            <label for="Message">Message : </label>
                <textarea name="Message" id="Message" placeholder="Tapez ici votre message" required></textarea> <br>

            <input type="submit" value="Envoyer" onclick="checkForm()">

        </form>
    </div>

</body>

</html>
