<?php
    require_once 'php/connexion.php';
    require_once 'php/header.php';
    require_once 'php/head.php';
    require_once 'php/footer.php';
    require_once 'php/leftNavbar.php';
    require_once 'php/varSession.inc.php';
    require_once 'php/db.php';
    //require_once 'panier.php';
?>

<!DOCTYPE html>
<script src="http://code.jquery.com/jquery.js"></script>
<html>
    <body>
        <?php
            if (isset($_GET['cat']) && isset($_SESSION['tabProduit'][0]['ref'])){
                $cat = $_GET['cat'];
        ?>
                <div class="tableContainer">
                    <table class="tableChild">
                        <tr>
                            <th>Aperçu</th>
                            <th>Modèle</th>
                            <th>Prix</th>
                            <th>Référence</th>
                        </tr>
        <?php
                $i=0;
                while(!empty($_SESSION['tabProduit'][$i]['ref'])){
                $url=$_SESSION['tabProduit'][$i]['urlImage'];
                $url=str_replace("'", "", $url);
                $url=str_replace(" ", "", $url);
                $url="background-size: 100%; background-image: url(https://drive.google.com/uc?export=view&id=".$url ;
        ?>
                        <tr class="line">
                            <td class="imgTab" style="<?php echo $url ?>"  onclick="zoomImage(<?php echo $_SESSION['tabProduit'][$i]['urlImage'] ?>)"></td>
                            <td> <?php echo $_SESSION['tabProduit'][$i]['nom'] ?> </td>
                            <td><NOBR> <?php echo $_SESSION['tabProduit'][$i]['prix'] ?> € </NOBR></td>
                            <td> <?php echo $_SESSION['tabProduit'][$i]['ref'] ?> </td>
                        </tr>
        <?php
                $i+=1;
                }
        ?>
                    </table>
                    <table id="tableStockCmd">
                        <tr>
                            <th class="displayElement">Stock</th>
                            <th class="displayElement">Commande</th>
                        </tr>
        <?php
                $i=0;
                while(!empty($_SESSION['tabProduit'][$i]['ref'])){
        ?>
                        <tr class="line">
                            <td class="displayElement" id="stock"> <?php echo $_SESSION['tabProduit'][$i]['stock'] ?> </td>
                            <td class="displayElement" id="commande">
                                    <input type="button" value="-" id="btnMoins" onclick="removeItem(<?php echo $i ?>);">
                                    <input type="text" readonly="true" id="<?php echo "item" . $i ?>" class="ctrText" value="0"> 
                                    <input type="button" value="+" id="btnPlus" onclick="addItem(<?php echo $i ?>);">
                                    <input type="button" value="Ajouter au panier" class="btnPanier" onclick="ajouterPanier(<?php echo $i ?>,'<?php echo $_SESSION['tabProduit'][$i]['ref'] ?>')">
                            </td>
                        </tr>
        <?php
                $i+=1;
                }
        ?>
                    </table>

            </div>

            <div id="afficherStock">
                <input type="button" onclick="displayStock()" value="Afficher le stock" id="btnStock">
            </div>


            <div id="zoomImg">
                <div id="imgZ">
                    <input type="button" value="Fermer l'image" id="fermerImg" onclick="fermerImage()">
                </div>
            </div>

            <script>
                var x = document.getElementsByClassName('displayElement');
                var tailleListe = x.length;
                for (i = 0; i < tailleListe; i++) {
                    x[i].style.display = 'none';
                }
            </script>

        <?php
                }
                if(!isset($_SESSION['tabProduit'][0]['ref'])){
        ?>
                    <div class="profil">
                        <div class="deconnecte">
                            La  catégorie n'éxiste pas...<br>
                        </div>
                    </div>
        <?php
                }
        ?>
    </body>

</html>