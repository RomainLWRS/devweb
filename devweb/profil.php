<?php
    require_once 'php/connexion.php';
    require_once 'php/header.php';
    require_once 'php/head.php';
    require_once 'php/footer.php';
    require_once 'php/leftNavbar.php';
    require_once 'php/varSession.inc.php';
    require_once 'php/db.php';
    //require_once 'panier.php';


    foreach($_POST as $key=>$val)
    ${$key}=$val;
    $req = NULL;
    $exec = NULL;
    $profilErreur = NULL;

    if(isset($modifier)){
        if(!preg_match("#^[a-zA-Z \-%*']+$#",$nom) && !empty($nom)){
            $profilErreur.="⛔ Nom invalide";
        }
        elseif(!preg_match("#^[a-zA-Z \-]+$#",$prenom) && !empty($prenom)){
            $profilErreur.="<p> ⛔ Prénom invalide</p>";
        }
        elseif(!empty($newPassword) && empty($password)){
            $profilErreur.="<p> ⛔ Si vous voulez changer de mot de passe, merci de renseigner l'ancien</p>";
        }
        elseif($newPassword!=$confirmNewPassword){
            $profilErreur.="<p> ⛔ Nouveau mot de passe non identique !</p>";
        }
        elseif(!preg_match("#[0-9]#",$tel) && !empty($tel)){
            $profilErreur.="<p> ⛔ Mauvais format tel</p>";
        }
        elseif(!preg_match("#[0-9]#",$postal) && !empty($postal)){
            $profilErreur.="<p> ⛔ Mauvais format code postal</p>";
        }
        elseif(!preg_match("#^[a-zA-Z \-%*']+$#",$ville) && !empty($ville)){
            $profilErreur.="<p> ⛔ Mauvais nom de ville</p>";
        }
        elseif ($_SESSION['password']!=md5($password) && (!empty(md5($password) && !empty($newPassword)))){
            echo $password . '  ';
            echo $_SESSION['password'];
            $profilErreur.="<p> ⛔ Votre mot de passe actuel n'est pas bon !</p>";
        }
        else{
            if($_SESSION['id']==NULL){
                $profilErreur.="<p> ⛔ Oups ! Un erreur s'est produite... </p>";
            }else{
                if(!empty($nom)){
                    $req .= " nom = ?,";
                    $exec .= "$nom,";
                }
                if(!empty($prenom)){
                    $req .= " prenom = ?,";
                    $exec .= "$prenom,";
                }
                if(!empty($dateNaissance)){
                    $req .= " dateNaissance = ?,";
                    $exec .= "$dateNaissance,";
                }
                if(!empty($tel)){
                    $req .= " telephone = ?,";
                    $exec .= "$tel,";
                }
                if(!empty($adresse)){
                    $req .= " adresse = ?,";
                    $exec .= "$adresse,";
                }
                if(!empty($postal)){
                    $req .= " departement = ?,";
                    $exec .= "$postal,";
                }
                if(!empty($ville)){
                    $req .= " ville = ?,";
                    $exec .= "$ville,";
                }
                if(!empty($newPassword)){
                    $req .= " password = ?,";
                    $exec .= md5($newPassword) . ",";
                }

                if($req!= NULL){

                $req = substr($req,0,-1); //supprime la dernière virgule
                $req = "UPDATE user SET" . $req . " WHERE id = ?";

                $id = $_SESSION['id']; //permet la concaténation suivante
                $exec = $exec . "$id";

                $tabExec=explode(",",$exec); //crréation d'un tableau qui permet d'éxecuter la req

                $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $ins = $db->prepare($req);
                $ins -> setFetchMode(PDO::FETCH_ASSOC);
                $ins -> execute($tabExec);

                header('Location: php/deconnexion.php');
                exit;
                }else{
                    $profilErreur .= "<p> ⛔ Rien n'a été modifié... </p>";
                }
                
            }
        }
    }
?>


<!DOCTYPE html>
<html lang="en">
    <body>
    <?php if(!empty($_SESSION['online'])){ ?> <!-- On vérifie si une session existe, dans ce cas, l'utilisateur peut acceder à son profil. --> 
        <div class="profil">
            <div class="profilContainer">
                <form name="profilForm" method="post" >
                    <div class="">
                    <label for="nom"> Nom : <?php echo@$_SESSION['nom']?> </label>
                        <input type="text" name="nom" placeholder="Modifier nom" value="<?php echo@$nom?>"/> <br>

                    <label for="prenom"> Prénom : <?php echo $_SESSION['prenom']?></label>
                        <input type="text" name="prenom" placeholder="Modifier prénom" value="<?php echo@$prenom?>"/> <br>

                    <label for="dateNaissance"> Date de naissance : <?php echo $_SESSION['dateNaissance']?> </label>
                        <input type="date" name="dateNaissance" id="dateNaissance" value="<?php echo@$dateNaissance?>"/> <br>

                    <label for="email"> Adresse email : <?php echo $_SESSION['email']?> </label>
                        <input type="email" name="email" placeholder="Pour modifier votre adresse email, contactez-nous" value="<?php echo@$email?> " readonly/> <br>

                    <label for="tel"> Numéro de téléphone : <?php echo $_SESSION['tel']?></label>
                        <input type="tel" name="tel"  placeholder="Modifier numéro de téléphone" minlength="10" maxlength="10" value="<?php echo@$tel?>"/> <br>

                    <label for="adresse"> Adresse : <?php echo $_SESSION['adresse']?></label>
                        <input type="text" name="adresse"  placeholder="Modifier adresse" value="<?php echo@$adresse?>" /> <br>

                    <label for="postal"> Code postal : <?php echo $_SESSION['departement']?></label>
                        <input type="tel" name="postal"  placeholder="Modifier code postal" minlength="5" maxlength="5" value="<?php echo@$postal?>"/> <br>

                    <label for="ville"> Ville : <?php echo $_SESSION['ville']?> </label>
                        <input type="text" name="ville"  placeholder="Modifier ville" value="<?php echo@$ville?>" /> <br>

                    <label for="password"> Mot de passe actuel</label>
                        <input type="password"  value="<?php echo@$password?>" name="password" placeholder="Mot de passe" minlength="8" /> <br>

                    <label for="password"> Nouveau mot de passe </label>
                        <input type="password"  value="<?php echo@$newPassword?>" name="newPassword" placeholder="Nouveau mot de passe (8 caractères minimum)" minlength="8" /> <br>

                    <label for="confirmPassword"> Confirmer le mot de passe </label>    
                        <input type="password" value="<?php echo@$confirmNewPassword?>" name="confirmNewPassword" placeholder="Confirmer le nouveau mot de passe" /> <br>

                    </div>
                    <input type="submit" name="modifier" value="Modifier"/>  
                </form>
            </div>
            <?php if(!empty($profilErreur)) { ?>
                        <div class="erreur">
                            <?=$profilErreur?>
                        </div>
            <?php } ?>
        </div>
    <?php }else{ ?>
        <div class="profil">
            <div class="deconnecte">
                Vous n'êtes pas connecté<br>
                Pour acceder à votre profil, connectez-vous
            </div>
        </div>
    <?php } ?>
    </body>
</html>
