var stockVisibility = true;

function displayStock() {
    var x = document.getElementsByClassName('displayElement');
    var lengthTab = x.length;
    var i;
    var btn = document.getElementById("btnStock");

    if (btn.value=="Masquer le stock"){
        btn.value = 'Afficher le stock';
    }
    else{
        btn.value = 'Masquer le stock';
    }
    if (stockVisibility == true) {
        stockVisibility = false;
        for (i = 0; i < lengthTab; i++) {
            x[i].style.display = 'table-cell';
        }

    } else {
        stockVisibility = true;
        var elm = document.getElementById(afficherStock);
        for (i = 0; i < lengthTab; i++) {
            x[i].style.display = 'none';
        }
    }
}

function addItem(n){
    
    var iNumber = parseInt(document.getElementById("item"+n).value);
    var maxStock = document.getElementById("item"+n).parentElement.previousElementSibling.innerHTML;
    
    if (iNumber < maxStock){
        iNumber += 1;
    }
    document.getElementById("item"+n).value = iNumber;
}

function removeItem(n){
    var iNumber = parseInt(document.getElementById("item"+n).value);
    if (iNumber > 0){
    iNumber -= 1;
    }
    document.getElementById("item"+n).value = iNumber;
}

function ajouterPanier(n,ref){
    var maxStock = document.getElementById("item"+n).parentElement.previousElementSibling.innerHTML;
    var quantiteCommande = document.getElementById("item"+n).value
    var newStock = maxStock-quantiteCommande;
    $.ajax({
        type: 'POST',
        url: 'php/variationStock.php',
        data : "ref=" + ref + "&newstock=" + newStock,
        success: function(data){
            // Si le stock max est inférieur à 0 alors on met un message d'erreur et le stock reste inchangé
            alert("Vous avez bien commandé " + quantiteCommande + " [" + ref + "]");
            father.children[4].textContent = data;
            father.children[5].children[1].value = 0;
            }
      });
    location.reload()
}

function zoomImage(img){  
    var x = document.getElementById("zoomImg");
    var z = document.getElementById("imgZ");
    x.style.display = 'block' ;
    z.style.backgroundImage = 'url(https://drive.google.com/uc?export=view&id=' + img + ')' ;
}

function fermerImage(){
    var x = document.getElementById("zoomImg");
    var z = document.getElementById("imgZ");
    x.style.display = 'none' ;
    z.style.backgroundImage = '' ;
}

function ouvrirPanier(){  
    var x = document.getElementById("displayPanier");
    var z = document.getElementById("panierContainer");
    x.style.display = 'block' ;
}

function fermerPanier(){
    var x = document.getElementById("displayPanier");
    var z = document.getElementById("panierContainer");
    x.style.display = 'none' ;
}