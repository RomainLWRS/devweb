<?php
    require_once 'php/connexion.php';
    require_once 'php/header.php';
    require_once 'php/head.php';
    require_once 'php/footer.php';
    require_once 'php/leftNavbar.php';
    require_once 'php/varSession.inc.php';
    require_once 'php/db.php';
    //require_once 'panier.php';
?>

<!DOCTYPE html>
<html>


<body>
    <div class="mainContainer">
        <div class="diapoContainer">
            <img src="https://drive.google.com/uc?export=view&id=1CIl-nWJXiPAlkZjCN1nCwc_cpfu67XJV">
            <img src="https://drive.google.com/uc?export=view&id=1fl6Cb850r2lriDt5RM8Teza8XKRtFEwj">
            <img src="https://drive.google.com/uc?export=view&id=1ynpn31p8cL8GBo1M5cff65XiLu2ux7CN">
            <img src="https://drive.google.com/uc?export=view&id=1O0QQgBJL-KK3N5RQqyUCjpoPM_CW0vBf">
            <img src="https://drive.google.com/uc?export=view&id=1a3z6ft2QKQbtBXpW40t7E9FGEykbZsjH">
            <img src="https://drive.google.com/uc?export=view&id=1upzTPYqMx2JesKF0IaLUYzIFk_gEyeiW">
            <img src="https://drive.google.com/uc?export=view&id=1OQmBYwyBu1Clf3kwAi5FP0314NC_D8Be">
            <img src="https://drive.google.com/uc?export=view&id=1851pD62SACTIeyN2C7hUohG0SMgtzUCY">
            <img src="https://drive.google.com/uc?export=view&id=12Vp_ZBVnT4-b5ZLJVdiO2i6RqkEeQLyf">
            <img src="https://drive.google.com/uc?export=view&id=15SOQPfmkprf0xapfLlVJ2n5nJCmpAXSr">
        </div>
        <div class="actuContainer">
            <img src="https://drive.google.com/uc?export=view&id=1Mo08-QVCxUQng1enOATLyNiHgbURrj_t">
            <h3> Lamborghini SC20 (2021) </h3>
                <p>
                    Nouvelle Lamborghini SC20 (2021) – Les one-off, ces modèles réalisés en exemplaire unique, ont décidément le vent en poupe. Un richissime client de la firme de Sant’Agata Bolognese a demandé à ses ingénieurs de transformer l’Aventador en une barquette dénuée de pare-brise. Avec tous les moustiques qui s’échoueront sur son ratelier, on lui souhaite d’ors et déjà bon appétit, malgré le saute-vent qui équipe son bolide.
                    <br> <br>
                </p>
            <h4> Descendante de l’Aventador J </h4>
                <p>
                    Après avoir révélé, il y a quelques semaines, l’Essenza SCV12, un modèle exclusif destiné à la piste, Lamborghini clôt cette année 2020 avec l’impressionnante SC20. Malgré sa filiation avec l’Aventador, la belle ne partage quasiment aucun élément de carrosserie, mais fait référence au concept-car Aventador J de 2012 qui se caractérisait déjà par son architecture de barquette.
                    Mitja Borkert, à la tête du centre de style Lamborghini, dit à son sujet :« Deux ans après la SC18 Alston, la SC20 est un nouveau défi. Les principales sources d’inspiration ont été la Diablo VT Roadster, l’Aventador J, la Veneno Roadster et la Concept S : le résultat est une combinaison sensationnelle de créativité et d’attitude de course ».
                </p>
            <h4> Prix Lamborghini SC20 </h4>
                <p>
                Pour catapulter la Lamborghini SC20, rien de très original en revanche, mais de l’efficacité, avec le V12 6.5 l de 770 ch et 720 Nm de couple. Les 4 roues motrices sont de rigueur pour espérer dompter ce monstre de puissance qu’il ne faut surtout pas casser ! Pour autant, le tarif de ce modèle unique, puisque produit en un seul exemplaire, demeure secret. Tout comme le nom de son heureux propriétaire, bien évidemment.
                </p>
            <div class="actuAuthor"> Julien JODRY (Auto Moto) - 16/12/2020 </div>
        </div>

        <div class="contactContainer">
            <div class="elementContact"> <NOBR>
                <img src="https://drive.google.com/uc?export=view&id=1K4JXeENqI50_PdIgC0oluNV1Zb_ZGLQ2"> <NOBR>
                <p> kiroule.com@contact.fr</p> <NOBR>
                <img src="https://drive.google.com/uc?export=view&id=13nYSMCnZ7xGwJ0tVhZ7Bs_b9rVd4dcQs"> <NOBR>
                <p> 01.23.45.67.89</p> <NOBR>
                <img src="https://drive.google.com/uc?export=view&id=14rQ-_Hs2XKPCOacgek9JEogxuBnH-tqS"> <NOBR>
                <p> @Kiroule.com</p> <NOBR>
            </div> 
        </div>
    </div>
</body>

</html>