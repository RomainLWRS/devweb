<?php
    require_once 'php/head.php';
    require_once 'php/footer.php';
    require_once 'php/varSession.inc.php';
    require_once 'php/db.php';

    foreach($_POST as $key=>$val)
    ${$key}=$val;
    $erreur='';

    if(isset($valider)){
        if(!preg_match("#^[a-zA-Z \-%*']+$#",$nom)){
            $erreur.="<p>⛔ Nom invalide</p>";
        }
        elseif(!preg_match("#^[a-zA-Z \-]+$#",$prenom)){
            $erreur.="<p>⛔ Prénom invalide</p>";
        }
        elseif(!preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#",$email)){
            $erreur.="<p>⛔ Format d'adresse email Invalide</p>";
        }
        elseif(empty($password)){
            $erreur.="<p>⛔ Mot de passe vide</p>";
        }
        elseif(empty($dateNaissance)){
            $erreur.="<p>⛔ Date naissance non renseignée !</p>";
        }
        elseif($password!=$confirmPassword){
            $erreur.="<p>⛔ Mot de passe non identique</p>";
        }
        elseif(!preg_match("#[0-9]#",$tel)){
            $erreur.="<p>⛔ Mauvais format tel</p>";
        }
        elseif(!preg_match("#[0-9]#",$postal)){
            $erreur.="<p>⛔ Mauvais format code postal</p>";
        }
        elseif(!preg_match("#^[a-zA-Z \-%*']+$#",$ville)){
            $erreur.="<p>⛔ Mauvais nom de ville</p>";
        }
        else{
            $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $req = $db->prepare("SELECT id,nom FROM user WHERE email=? LIMIT 1");
            $req -> setFetchMode(PDO::FETCH_ASSOC);
            $req -> execute (array($email));
            $tab = $req->fetch();
            if($tab['id']!=NULL){
                $erreur.="<p>⛔ Un compte est déjà lier à cette adresse email... </p>";
            }else{
                $ins = $db->prepare("INSERT INTO user(prenom,nom,dateNaissance,email,telephone,adresse,departement,ville,password,validite) VALUES(?,?,?,?,?,?,?,?,?,?)");
                $ins -> execute(array($prenom,$nom,$dateNaissance,$email,$tel,$adresse,$postal,$ville,md5($password),1));
                sleep(4);
                header('Location: index.php');
                exit;
            }
            //fichier .txt
            //$fp=fopen("login.txt","a+");
            //fputs($fp,$nom.",".$prenom.",".$email.",".$tel.",".$postal.",".md5($password)."\n");
        }
    }
?>


<!DOCTYPE html>
<html lang="en">
<body>

<div class="inscription">
    <h1>Inscription</h1>
    <div class="inscriptionContainer">
        <form name="inscriptionForm" method="post" >
            <div class="">
            <label for="nom"> Nom </label>
                <input type="text" name="nom" placeholder="Nom" value="<?php echo@$nom?>"/> <br>

            <label for="prenom"> Prénom </label>
                <input type="text" name="prenom" placeholder="Prénom" value="<?php echo@$prenom?>"/> <br>

            <label for="dateNaissance"> Date de naissance </label>
                <input type="date" name="dateNaissance" id="dateNaissance" value="<?php echo@$dateNaissance?>"/> <br>

            <label for="email"> Adresse email </label>
                <input type="email" name="email" placeholder="Adresse email" value="<?php echo@$email?>"/> <br>

            <label for="tel"> Numéro de téléphone </label>
                <input type="tel" name="tel"  placeholder="Numéro de téléphone" minlength="10" maxlength="10" value="<?php echo@$tel?>"/> <br>

            <label for="adresse"> Adresse </label>
                <input type="text" name="adresse"  placeholder="Adresse" value="<?php echo@$adresse?>" /> <br>

            <label for="postal"> Code postal </label>
                <input type="tel" name="postal"  placeholder="Code postal" minlength="5" maxlength="5" value="<?php echo@$postal?>"/> <br>

            <label for="ville"> Ville </label>
                <input type="text" name="ville"  placeholder="Ville" value="<?php echo@$ville?>" /> <br>

            <label for="password"> Mot de passe </label>
                <input type="password"  value="<?php echo@$password?>" name="password" placeholder="Mot de passe (8 caractères minimum)" minlength="8" /> <br>

            <label for="confirmPassword"> Confirmer le mot de passe </label>    
                <input type="password" value="<?php echo@$confirmPassword?>" name="confirmPassword" placeholder="Confirmer mot de passe" /> <br>

            </div>
            <input type="submit" name="valider" value="M'inscrire" />
            ou
            <a href="index.php"><input type="button" name="return" value="J'ai déjà un compte"></a>   
        </form>
    </div>
    <?php if(!empty($erreur)) { ?>
                <div class="erreur">
                    <?=$erreur?>
                </div>
    <?php } ?>

</div>

</body>
</html>