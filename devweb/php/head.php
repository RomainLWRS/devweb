<head>

<?php

include_once 'db.php';

$catProduit = $db->prepare("SELECT DISTINCT categorie FROM produit");
$catProduit -> setFetchMode(PDO::FETCH_ASSOC);
$catProduit -> execute();
$tabCat = $catProduit->fetchAll();
$nbCat = $catProduit->rowCount();
$i = NULL;

if (isset($_GET['cat'])){
    $cat = $_GET['cat'] ;
?>
    <title> Kiroule.com | <?php echo ucwords($cat) ?> </title>
<?php
} else {
    $cat='';
}
?>

<?php 

    if(basename($_SERVER['PHP_SELF'], ".php")=='index'){
?>
        <title> Kiroule.com | Accueil</title>
<?php
    }
    if(basename($_SERVER['PHP_SELF'], ".php")=='profil'){
?>
        <title> Kiroule.com | <?php if(isset($_SESSION['online']))echo $_SESSION['nom'] . " " .$_SESSION['prenom'];?> </title>
<?php
    }
    if(basename($_SERVER['PHP_SELF'], ".php")=='contact'){
        ?>
                <title> Kiroule.com | Contact </title>
        <?php
    }
?>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/script.js"></script>
</head>
