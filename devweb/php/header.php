<header>
    <img class="logo" src="https://drive.google.com/uc?export=view&id=1Wun92x_94hgbLqd7dZGc_QyPKvGn4cXj">
    <div id="elementHeader">
        <h1>Kiroule.com</h1>
        <h2>Vente et location de voitures de luxe</h2>
        <ul id="menu_horizontal">
            <li class="menuH_container accueil" >
                <a href="index.php">Accueil</a> 
                <div class="descAccueil">Retourner sur la page d'accueil</div>
            </li>
<?php
if (isset($_GET['cat'])){
    $cat = $_GET['cat'] ;
} else {
$cat='';
}
    if($cat != 'lamborghini'){
?>
            <li class="menuH_container lambo">
                <a href="produits.php?cat=lamborghini">Lamborghini</a> 
                <div class="descLambo">Lamborghini Gallardo LP 550-2</div>
            </li>
<?php
    }
    if ($cat != 'astonmartin'){
?>
            <li class="menuH_container aston">
                <a href="produits.php?cat=astonmartin">Aston Martin</a> 
                <div class="descAston">Aston Martin One-77</div>
            </li>
<?php
    }
    if ($cat != 'ferrari'){
?>
            <li class="menuH_container ferrari" >
                <a href="produits.php?cat=ferrari">Ferrari</a> 
                <div class="descFerrari">Ferrari 488 Spider 2015</div>
            </li>
<?php
    }
?>
            <li class="menuH_container contact" >
                <a href="contact.php">Contact</a> 
                <div class="descContact">Contacter le service client</div>
            </li>
        </ul>
    </div>
</header>