<?php

    require_once 'db.php';

    $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    $uptd = $db->prepare('UPDATE produit SET stock = ? WHERE ref = ?');
    $uptd -> setFetchMode(PDO::FETCH_ASSOC);
    $tab = [$_POST['newstock'],$_POST['ref']];
    $uptd -> execute($tab);

?>
