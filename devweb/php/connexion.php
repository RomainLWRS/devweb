<?php
    session_start();
    include 'db.php';
    include 'panier.php';
    foreach($_POST as $key=>$val) ${$key}=$val;
    $erreur="<p style='color:red;text-shadow: 0 0 0.1em black, 0 0 0.1em black,0 0 0.1em black;'>";

    if(isset($connexion)){
        $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        $verifMailMdp = $db->prepare("SELECT * FROM user WHERE email LIKE ? AND password LIKE ?");
        $verifMailMdp -> setFetchMode(PDO::FETCH_ASSOC);
        $verifMailMdp -> execute(array($email,md5($password)));
        $tabUser = $verifMailMdp->fetch();

        $exist = $db->prepare("SELECT id FROM user WHERE email LIKE ?");
        $exist -> setFetchMode(PDO::FETCH_ASSOC);
        $exist -> execute(array($email));
        $tabExist = $exist->fetch();

        $nbProduit = $db->prepare("SELECT * FROM produit");
        $nbProduit -> setFetchMode(PDO::FETCH_ASSOC);
        $nbProduit -> execute();
        $nbProduit = $nbProduit->rowCount();

        print_r($tabUser);
        if($tabUser['id']!=NULL){ //si cette condition n'est pas respectée cela implique que la table est "vide" et donc que l'email/mdp ne correspondent pas.
            $_SESSION['online']=1;
            $_SESSION['id']=$tabUser['id'];
            $_SESSION['prenom']=$tabUser['prenom'];
            $_SESSION['nom']=$tabUser['nom'];
            $_SESSION['dateNaissance']=$tabUser['dateNaissance'];
            $_SESSION['email']=$tabUser['email'];
            $_SESSION['tel']=$tabUser['telephone'];
            $_SESSION['adresse']=$tabUser['adresse'];
            $_SESSION['departement']=$tabUser['departement'];
            $_SESSION['ville']=$tabUser['ville'];
            $_SESSION['password']=$tabUser['password'];
            $_SESSION['validite']=$tabUser['validite'];
            $_SESSION['nbProduit']=$nbProduit;

            $_SERVER['QUERY_STRING'] != "" ? $redirection = $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'] : $redirection = $_SERVER['PHP_SELF']; //La redirection se fait sur la page actuelle.
            header("Location: ".$redirection.""); //redirection
        }elseif($tabExist['id']!=NULL) {
            $erreur.="⛔ Mot de passe incorect ! </p>";
        }else {
            $erreur.="⛔ Compte innexistant ! </p>";
        }

        // UTILISATION DU FICHIER TXT //
        /*$fp=fopen("login.txt","r");
        while(!feof($fp)){
            $str=fgets($fp);
            $tab=explode(",",$str);
            if($email==@$tab[2] && $password==substr(@$tab[5],0,32)){
                $_SESSION["online"]="oui";
                $_SESSION["nomPrenom"]=strtoupper($tab[0]." ".$tab[1]);
                $_SESSION["email"]=strtolower($tab[2]);
                $_SESSION["tel"]=$tab[3];
                $_SERVER['QUERY_STRING'] != "" ? $redirection = $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'] : $redirection = $_SERVER['PHP_SELF'];
                header("Location: ".$redirection."");
            }
        }*/
    }
?>

<!DOCTYPE html>
<html lang="en">
<body>
    <?php if(isset($_SESSION['online'])!=1){ ?>
        <div class="connexion">
            <h1>Connexion</h1>
            <form name="connexionForm" method="post">
                    <input type="email" name="email" placeholder="Adresse e-mail" value="<?php echo @$email?>"/>
                    <input type="password"  value="<?php echo @$password?>" name="password" placeholder="Mot de passe"/>
                    <br><br>
                <input type="submit" name="connexion" value="Connexion"/>
                ou
                <a href="inscription.php"><input type="button" name="inscription" value="Inscription"></a>

                <?php if(!empty($erreur)) { ?>
                    <div>
                        <?=$erreur?>
                    </div>
                <?php 
                }
                ?>
    
            </form>
        </div>
    <?php } else{ ?>
        <div class="deconnexion">
            <form method="get">
                <h1><?php echo strtoupper($_SESSION['nom'] . ' ' . $_SESSION['prenom'])?></h1>
                <a href="php/deconnexion.php"><input type="button" name="deconnexion" value="Se deconnecter 🚫"></a>
                <a href="profil.php"><input type="button" name="profil" value="Profil 🧍"></a>
                <input type="button" name="panier" value="Acceder au panier 🛒" onclick="">
            </form>
        </div>



    <?php } ?>
</body>
</html>