-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3307
-- Généré le :  mar. 27 avr. 2021 à 08:56
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `devweb`
--

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `nom` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `prix` int(11) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `urlImage` varchar(255) NOT NULL,
  PRIMARY KEY (`ref`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`nom`, `stock`, `prix`, `ref`, `categorie`, `urlImage`) VALUES
(' Vulcan ', 3, 4000000, 'am06', 'astonmartin', ' \'1uU7_BbnxmBaDmCW-ZhmjL6k1Yq4NwkvW\' '),
(' 458 Speciale ', 1, 235000, 'fr01', 'ferrari', ' \'1FeKj6ANx5bhAH_J7vrx_ymSnd7uEC2Vx\' '),
(' Roma ', 2, 200000, 'fr02', 'ferrari', ' \'1VivhB8__oY6aLoYlihcf4pY43q1HdGL2\' '),
(' F40 ', 3, 1580000, 'fr03', 'ferrari', ' \'1asOr9Op4elD4eKM6hOtf6uuqrgEeu76U\' '),
(' 812 Superfast GTR ', 4, 325000, 'fr04', 'ferrari', ' \'1_6DqX718xfwv3Yuc9AmslPWBlVw3nkMj\' '),
(' SC18 Alston ', 0, 6400000, 'lb01', 'lamborghini', ' \'10ugG5yTryeqvSyRYUOs0UfReg71OmMKk\' '),
(' Aventador SV Coupe ', 0, 550000, 'lb02', 'lamborghini', ' \'1Vgtsv1r32kRNM3zxLi3XSYekH4U88maC\' '),
(' Huricane ', 0, 180000, 'lb03', 'lamborghini', ' \'1dbMYbhXaoWcoalpPqc8eRWfypWz_jTpw\' '),
(' Urus ', 0, 210000, 'lb04', 'lamborghini', ' \'1QP7Kia-aeMbLLOxNg3D1ypTxHbeE5QqC\' '),
(' Sian ', 5, 2100000, 'lb05', 'lamborghini', ' \'1AUdNnAWPVpAHpPmiHj0QKyE51l6GnEgz\' '),
(' Veneno ', 6, 7000000, 'lb06', 'lamborghini', ' \'1YW_0n-lYaXrC9nXDk4jJ8XmoHty8dOUP\' '),
(' DB11 Volante ', 1, 204000, 'am01', 'astonmartin', ' \'1UqgmTlP15pwGN2d-wXll-yp3FCJ32_YC\' '),
(' V8 Vantage ', 2, 120000, 'am02', 'astonmartin', ' \'1Yw8bofmVdrCYptYDfLzHcx0esGkVtM_Z\' '),
(' DBX ', 3, 200000, 'am03', 'astonmartin', ' \'12N-G3m7jwD3rynaj8K_aWq7eEDL5p7Gm\' '),
(' Vanquish ', 4, 300000, 'am04', 'astonmartin', ' \'15HKqF60ew2OrY2a1sE4GrBTEB9qacWh1\' '),
(' Valkyrie AMR Pro ', 5, 3600000, 'am05', 'astonmartin', ' \'1g6FqiTqfEU4MNykDW06Hbs--o8uQ3LFY\' '),
(' F8 Tributo Spider ', 5, 250000, 'fr05', 'ferrari', ' \'1z_XbZYaxCXCWLXgyUwCtz66iRFODMnII\' '),
(' California T ', 6, 190000, 'fr06', 'ferrari', ' \'10V2BnK7u-AC-4_mesOdAQMSLeViKNeIF\' ');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `dateNaissance` varchar(10) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `departement` varchar(5) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `validite` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `prenom`, `nom`, `dateNaissance`, `email`, `telephone`, `adresse`, `departement`, `ville`, `password`, `validite`) VALUES
(0, 'Romain', 'LAUWERS', '1999-01-30', 'lauwersrom@cy-tech.fr', '0123456789', '9 rue des fleurs', '75000', 'Paris', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', 1),
(1, 'Louis', 'DUPOND', '2000-01-20', 'l.d@gmail.com', '0192010498', '12 rue du levier', '95000', 'Cergy', 'aa36dc6e81e2ac7ad03e12fedcb6a2c0', 1),
(2, 'David', 'JACKSON', '2021-04-08', 'pat@sfr.fr', '0192010498', '12 rue du levier', '95000', 'Cergy', '098f6bcd4621d373cade4e832627b4f6', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
